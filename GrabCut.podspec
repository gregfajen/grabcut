#
# Be sure to run `pod lib lint GrabCut.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GrabCut'
  s.version          = '0.3.1'
  s.summary          = 'Wrapper for OpenCV‘s GrabCut algorithm'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/gregfajen/GrabCut'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Greg Fajen' => 'gregfajen@gmail.com' }
  s.source           = { :git => 'https://github.com/gregfajen/GrabCut.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'GrabCut/Classes/**/*'
  
  # s.resource_bundles = {
  #   'GrabCut' => ['GrabCut/Assets/*.png']
  # }
  s.ios.vendored_frameworks = 'GrabCut/Frameworks/opencv2.framework'
  s.public_header_files = 'GrabCut/Classes/**/*.h'
  s.private_header_files = 'GrabCut/Classes/**/*.hpp'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
