# GrabCut

[![CI Status](https://img.shields.io/travis/Greg Fajen/GrabCut.svg?style=flat)](https://travis-ci.org/Greg Fajen/GrabCut)
[![Version](https://img.shields.io/cocoapods/v/GrabCut.svg?style=flat)](https://cocoapods.org/pods/GrabCut)
[![License](https://img.shields.io/cocoapods/l/GrabCut.svg?style=flat)](https://cocoapods.org/pods/GrabCut)
[![Platform](https://img.shields.io/cocoapods/p/GrabCut.svg?style=flat)](https://cocoapods.org/pods/GrabCut)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GrabCut is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GrabCut'
```

## Author

Greg Fajen, gregfajen@gmail.com

## License

GrabCut is available under the MIT license. See the LICENSE file for more info.
