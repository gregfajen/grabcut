//
//  CVImage.mm
//  GrabCutTest
//
//  Created by Greg on 10/12/18.
//

#import "CVImage.h"
#import "CVImageInternal.hpp"
#import <opencv2/core.hpp>
#import <opencv2/imgproc.hpp>
#import <opencv2/imgproc/types_c.h>


int CVColorModeToType(CVColorMode mode) {
    switch (mode) {
        case CVColorModeGrayScale:
            return CV_8UC1;
        case CVColorModeBGR:
            return CV_8UC3;
        case CVColorModeBGRA:
            return CV_8UC4;
            
        default:
            return 0;
    }
}

CVColorMode CVTypeToColorMode(int type) {
    switch (type) {
        case CV_8UC1:
            return CVColorModeGrayScale;
        case CV_8UC3:
            return CVColorModeBGR;
        case CV_8UC4:
            return CVColorModeBGRA;
            
        default:
            return CVColorModeGrayScale;
    }
}

@interface CVImage () {
    cv::Mat mat;
    CVImage *cachedBGRA;
    //    CGContextRef cachedContext;
}



@end



@implementation CVImage

- (id) initWithColorMode: (CVColorMode) mode width: (int) width height: (int) height {
    self = [super init];
    
    int type = CVColorModeToType(mode);
    cv::Mat cvMat(height, width, type);
    mat = cvMat;
    
    return self;
}

- (id) initWithColorImage: (UIImage *) image {
    self = [super init];
    
    CGImageRef cgImage = image.CGImage;
    CGFloat scale = image.scale;
    CGFloat width = image.size.width * scale;
    CGFloat height = image.size.height * scale;
    
    cv::Mat cvMat(height, width, CV_8UC4);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    width,                      // Width of bitmap
                                                    height,                     // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaPremultipliedLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, width, height), cgImage);
    
    void *contextData = CGBitmapContextGetData(contextRef);
    
    CGContextRelease(contextRef);
    CGColorSpaceRelease(colorSpace);
    
    mat = cvMat;
    
    void *matData = mat.data;
    
    NSLog(@"context data: %p", contextData);
    NSLog(@"mat data: %p", matData);
    
    return self;
}

- (id) initWithGrayscaleImage: (UIImage *) image {
    self = [super init];
    
    CGImageRef cgImage = image.CGImage;
    CGFloat scale = image.scale;
    CGFloat width = image.size.width * scale;
    CGFloat height = image.size.height * scale;
    
    cv::Mat cvMat(height, width, CV_8UC1);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    width,                      // Width of bitmap
                                                    height,                     // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNone |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, width, height), cgImage);
    CGContextRelease(contextRef);
    CGColorSpaceRelease(colorSpace);
    
    mat = cvMat;
    
    return self;
}

- (id) initWithMat: (cv::Mat) m {
    self = [super init];
    
    mat = m;
    
    return self;
}

- (cv::Mat) mat {
    return mat;
}

// MARK: Mat accessors

- (CVColorMode) colorMode {
    return CVTypeToColorMode( mat.type() );
}

- (int) width {
    return mat.cols;
}

- (int) height {
    return mat.rows;
}

- (int) stride {
    return (int)mat.step[0];
}

// MARK: Color Mode Conversions

- (CVImage *) asBGR {
    CVColorMode mode = self.colorMode;
    if (mode == CVColorModeBGR)
        return self;
    
    CVImage *BGR = [[CVImage alloc] initWithColorMode: CVColorModeBGR
                                                width: self.width
                                               height: self.height];
    
    if (mode == CVColorModeBGRA) {
        [self copyTo: BGR];
        return BGR;
    }
    
    
    int conversion = (mode == CVColorModeGrayScale) ? CV_GRAY2BGR : CV_BGRA2BGR;
    cvtColor(mat, BGR->mat, conversion);
    
    return BGR;
}

- (CVImage *) asBGRA {
    if (cachedBGRA != nil)
        return cachedBGRA;
    
    CVColorMode mode = self.colorMode;
    if (mode == CVColorModeBGRA)
        return self;
    
    CVImage *BGRA = [[CVImage alloc] initWithColorMode: CVColorModeBGRA
                                                 width: self.width
                                                height: self.height];
    
    if (mode == CVColorModeBGR) {
        [self copyTo: BGRA];
        return BGRA;
    }
    
    int conversion = (mode == CVColorModeGrayScale) ? CV_GRAY2BGRA : CV_BGR2BGRA;
    cvtColor(mat, BGRA->mat, conversion);
    
    return BGRA;
}

- (CVImage *) asGrayscale {
    CVColorMode mode = self.colorMode;
    if (mode == CVColorModeGrayScale)
        return self;
    
    CVImage *gray = [[CVImage alloc] initWithColorMode: CVColorModeGrayScale
                                                 width: self.width
                                                height: self.height];
    
    int conversion = (mode == CVColorModeBGRA) ? CV_BGRA2GRAY : CV_BGR2GRAY;
    cvtColor(mat, gray->mat, conversion);
    
    return gray;
}

- (CVImage *) alpha {
    CVImage *mask = [[CVImage alloc] initWithColorMode: CVColorModeGrayScale
                                                 width: self.width
                                                height: self.height];
    
    [self copyBGRAtoMask: mask];
    
    return mask;
}

- (void) copyTo: (CVImage *) target {
    switch (self.colorMode) {
        case CVColorModeBGRA:
            [self copyBGRAtoBGR: target];
            break;
            
        case CVColorModeBGR:
            [self copyBGRtoBGRA: target];
            break;
            
        default:
            break;
    }
}

- (void) copyBGRtoBGRA: (CVImage *) target {
    NSLog(@"copyBGRtoBGRA");
    UInt8 *iBytes = self->mat.data;
    UInt8 *oBytes = target->mat.data;
    
    int width = self.width;
    int height = self.height;
    int iStride = self.stride;
    int oStride = target.stride;
    
    assert(width == target.width);
    assert(height == target.height);
    assert(self.colorMode == CVColorModeBGR);
    assert(target.colorMode == CVColorModeBGRA);
    
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int i = iStride * y + x * 3;
            int o = oStride * y + x * 4;
            
            oBytes[o+0] = iBytes[i+0];
            oBytes[o+1] = iBytes[i+1];
            oBytes[o+2] = iBytes[i+2];
            oBytes[o+3] = 255;
        }
    }
}

- (void) copyBGRAtoBGR: (CVImage *) target {
    NSLog(@"copyBGRAtoBGR");
    UInt8 *iBytes = self->mat.data;
    UInt8 *oBytes = target->mat.data;
    
    int width = self.width;
    int height = self.height;
    int iStride = self.stride;
    int oStride = target.stride;
    
    assert(width == target.width);
    assert(height == target.height);
    assert(self.colorMode == CVColorModeBGRA);
    assert(target.colorMode == CVColorModeBGR);
    
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int i = iStride * y + x * 4;
            int o = oStride * y + x * 3;
            
            oBytes[o+0] = iBytes[i+0];
            oBytes[o+1] = iBytes[i+1];
            oBytes[o+2] = iBytes[i+2];
        }
    }
}

- (void) copyBGRAtoMask: (CVImage *) target {
    NSLog(@"copyBGRAtoMask");
    UInt8 *iBytes = self->mat.data;
    UInt8 *oBytes = target->mat.data;
    
    int width = self.width;
    int height = self.height;
    int iStride = self.stride;
    int oStride = target.stride;
    
    assert(width == target.width);
    assert(height == target.height);
    assert(self.colorMode == CVColorModeBGRA);
    assert(target.colorMode == CVColorModeGrayScale);
    
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int i = iStride * y + x * 4;
            int o = oStride * y + x * 1;
            
            oBytes[o] = iBytes[i+3];
        }
    }
}

// MARK: CGImage / UIImage

- (CVImage *) cachedBGRA {
    if (cachedBGRA != nil)
        return cachedBGRA;
    
    if (self.colorMode == CVColorModeBGRA)
        return self;
    
    cachedBGRA = self.asBGRA;
    return cachedBGRA;
}

- (CGContextRef) context {
    //    if (cachedContext != nil)
    //        return cachedContext;
    
    CVColorMode mode = self.colorMode;
    if (mode == CVColorModeBGR)
        return self.cachedBGRA.context;
    
    BOOL isGray = mode == CVColorModeGrayScale;
    
    
    CGColorSpaceRef colorSpace = isGray ? CGColorSpaceCreateDeviceGray() : CGColorSpaceCreateDeviceRGB();
    CGContextRef contextRef = CGBitmapContextCreate(mat.data,                   // Pointer to  data
                                                    mat.cols,                   // Width of bitmap
                                                    mat.rows,                   // Height of bitmap
                                                    8,                          // Bits per component
                                                    mat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    (isGray) ? kCGImageAlphaNone : kCGImageAlphaPremultipliedLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGColorSpaceRelease(colorSpace);
    CFAutorelease(contextRef);
    
    //    cachedContext = contextRef;
    return contextRef;
}

- (CGImageRef) cgImage {
    CGContextRef context = self.context;
    
    CGImageRef image = CGBitmapContextCreateImage(context);
    CFAutorelease(image);
    
    return image;
}

- (UIImage *) uiImage {
    return [[UIImage alloc] initWithCGImage: self.cgImage];
}

- (void) draw: (CVImageDrawingBlock) block {
    CGContextRef context = self.context;
    
    UIGraphicsPushContext(context);
    
    block();
    
    UIGraphicsPopContext();
}

//- (cv::Mat)cvMatFromUIImage:(UIImage *)image
//{
//    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
//    CGFloat cols = image.size.width;
//    CGFloat rows = image.size.height;
//
//    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
//
//    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
//                                                    cols,                       // Width of bitmap
//                                                    rows,                       // Height of bitmap
//                                                    8,                          // Bits per component
//                                                    cvMat.step[0],              // Bytes per row
//                                                    colorSpace,                 // Colorspace
//                                                    kCGImageAlphaNoneSkipLast |
//                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
//
//    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
//    CGContextRelease(contextRef);
//
//    return cvMat;
//}

//- (cv::Mat)cvMatGrayFromUIImage:(UIImage *)image
//{
//    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
//    CGFloat cols = image.size.width;
//    CGFloat rows = image.size.height;
//
//    cv::Mat cvMat(rows, cols, CV_8UC1); // 8 bits per component, 1 channels
//
//    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to data
//                                                    cols,                       // Width of bitmap
//                                                    rows,                       // Height of bitmap
//                                                    8,                          // Bits per component
//                                                    cvMat.step[0],              // Bytes per row
//                                                    colorSpace,                 // Colorspace
//                                                    kCGImageAlphaNoneSkipLast |
//                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
//
//    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
//    CGContextRelease(contextRef);
//
//    return cvMat;
//}

+ (void) test {
    UIImage *lenna = [UIImage imageNamed: @"lenna.png"];
    CVImage *cvImage = [[CVImage alloc] initWithColorImage: lenna].asBGR;
    cv::Mat img = cvImage->mat;
    
    cv::Mat1b markers(img.rows, img.cols);
    // let's set all of them to possible background first
    markers.setTo(cv::GC_PR_BGD);
    
    // cut out a small area in the middle of the image
    int m_rows = 0.1 * img.rows;
    int m_cols = 0.1 * img.cols;
    // of course here you could also use cv::Rect() instead of cv::Range to select
    // the region of interest
    cv::Mat1b fg_seed = markers(cv::Range(img.rows/2 - m_rows/2, img.rows/2 + m_rows/2),
                                cv::Range(img.cols/2 - m_cols/2, img.cols/2 + m_cols/2));
    // mark it as foreground
    fg_seed.setTo(cv::GC_FGD);
    
    // select first 5 rows of the image as background
    cv::Mat1b bg_seed = markers(cv::Range(0, 5),cv::Range::all());
    bg_seed.setTo(cv::GC_BGD);
    
    cv::Mat bgd, fgd;
    int iterations = 1;
    cv::grabCut(img, markers, cv::Rect(), bgd, fgd, iterations, cv::GC_INIT_WITH_MASK);
    
    // let's get all foreground and possible foreground pixels
    cv::Mat1b mask_fgpf = ( markers == cv::GC_FGD) | ( markers == cv::GC_PR_FGD);
    // and copy all the foreground-pixels to a temporary image
    cv::Mat3b tmp = cv::Mat3b::zeros(img.rows, img.cols);
    img.copyTo(tmp, mask_fgpf);
    // show it
    
    CVImage *input = [[CVImage alloc] initWithMat: img];
    CVImage *mask = [[CVImage alloc] initWithMat: markers];
    
    CVImage *temp = [[CVImage alloc] initWithMat: tmp];
    UIImage *result = temp.uiImage;
    
    CVImage *fgpf = [[CVImage alloc] initWithMat: mask_fgpf];
    
    [self saveImage: input.uiImage toFile: @"input.png"];
    [self saveImage: mask.uiImage toFile: @"mask.png"];
    [self saveImage: result toFile: @"result.png"];
    [self saveImage: fgpf.uiImage toFile: @"fgpf.png"];
}

+ (void) saveImage: (UIImage *) image toFile: (NSString *) filename {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = paths.firstObject;
    NSString *path = [basePath stringByAppendingFormat: @"/%@", filename];
    NSLog(@"saving to %@", path);
    
    NSData *data = UIImagePNGRepresentation(image);
    [data writeToFile: path atomically: YES];
}

@end


