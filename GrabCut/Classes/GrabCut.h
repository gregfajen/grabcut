//
//  GrabCut.h
//  GrabCutTest
//
//  Created by Greg on 10/18/18.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import "CVImage.h"
@class CVImage;

typedef enum {
    GrabCutPaintingModeDefiniteObject,
    GrabCutPaintingModeProbableObject,
    GrabCutPaintingModeProbableBackground,
    GrabCutPaintingModeDefiniteBackground
} GrabCutPaintingMode;

@interface GrabCut : NSObject {
    CGFloat initialScale;
    UIImageOrientation initialOrientation;
}

- (id _Nonnull) initWithImage: (UIImage *_Nonnull) image;

@property(readonly) CGFloat width;
@property(readonly) CGFloat height;

@property(readonly) CGFloat initialScale;
@property(readonly) UIImageOrientation initialOrientation;

@property(readonly, nonnull) CVImage *image;
@property(readonly, nonnull) CVImage *inMask;
@property(readonly, nonnull) CVImage *gcMask;
@property(readonly, nullable) CVImage *outMask;
@property(readonly, nonnull) CVImage *originalMask;
@property(readonly, nonnull) CVImage *displayImage;

- (void) updateDisplayImage;
//- (void) drawMask: (CVImageDrawingBlock _Nonnull) block;

- (void) iterate;

@end
