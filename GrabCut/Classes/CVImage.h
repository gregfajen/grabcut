//
//  CVImage.h
//  GrabCutTest
//
//  Created by Greg on 10/12/18.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^CVImageDrawingBlock) (void);

NS_ASSUME_NONNULL_BEGIN

typedef enum CVColorMode {
    CVColorModeGrayScale,
    CVColorModeBGR,
    CVColorModeBGRA
} CVColorMode;

@interface CVImage : NSObject

- (id) initWithColorMode: (CVColorMode) mode width: (int) width height: (int) height;
- (id) initWithColorImage: (UIImage *) image;
- (id) initWithGrayscaleImage: (UIImage *) image;
+ (void) test;

@property(readonly) CVColorMode colorMode;
@property(readonly) int width;
@property(readonly) int height;
@property(readonly) int stride;

@property(readonly) CVImage *asBGR;
@property(readonly) CVImage *asBGRA;
@property(readonly) CVImage *asGrayscale;
@property(readonly) CVImage *alpha;

@property(readonly) CGContextRef context;
@property(readonly) CGImageRef cgImage;
@property(readonly) UIImage *uiImage;
- (void) draw: (CVImageDrawingBlock) block;

@end

NS_ASSUME_NONNULL_END
