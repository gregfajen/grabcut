//
//  GrabCut.mm
//  GrabCutTest
//
//  Created by Greg on 10/18/18.
//

#import "GrabCut.h"
#import "CVImage.h"
#import "CVImageInternal.hpp"
#import <opencv2/core.hpp>
#import <opencv2/imgproc.hpp>
#import <opencv2/imgproc/types_c.h>

NS_INLINE UInt8 _displayToGrabCut(UInt8 d) {
    if (d > 200) {
        return cv::GC_FGD;
    }
    
    if (d > 150) {
        return cv::GC_PR_FGD;
    }
    
    if (d < 55) {
        return cv::GC_BGD;
    }
    
    return cv::GC_PR_BGD;
}

NS_INLINE CGSize _sizeForAspectRatioPixels(CGFloat aspect, CGFloat pixels) {
    CGFloat height = sqrt(pixels / aspect);
    CGFloat width = aspect * height;
    //    return CGSizeMake(round(width/8)*8, round(height/8)*8);
    return CGSizeMake(floor(width), floor(height));
}

NS_INLINE CGSize _sizeForSizeCapped(CGSize size, CGFloat maxMegapixels) {
    CGFloat megapixels = size.width * size.height;
    if (megapixels < maxMegapixels)
        return size;
    
    CGFloat aspect = size.width / size.height;
    return _sizeForAspectRatioPixels(aspect, maxMegapixels);
}

@implementation GrabCut

- (id) initWithImage: (UIImage *) image {
    self = [super init];
    
    CGFloat scale = image.scale;
    CGSize size = image.size;
    size.width *= scale;
    size.height *= scale;
    
    size = _sizeForSizeCapped(size, 1218816);
    
    
    initialScale = scale;
    initialOrientation = image.imageOrientation;
    
    NSLog(@"scale: %f", scale);
    NSLog(@"width: %f", size.width);
    NSLog(@"height: %f", size.height);
    NSLog(@" ");
    
    CVImage *cvImage = [[CVImage alloc] initWithColorMode: CVColorModeBGRA
                                                    width: size.width
                                                   height: size.height];
    [cvImage draw:^{
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGRect rect = CGRectMake(0, 0, size.width, size.height);
        CGContextDrawImage(context, rect, image.CGImage);
        
    }];
    
    [self saveImage: cvImage.uiImage toFile: @"image_rgba.png"];
    
    self->image = [cvImage asBGR];
    self->originalMask = [cvImage alpha];
    
    int w = self->image.width;
    int h = self->image.height;
    
    cv::Mat1b markers(h, w);
    markers.setTo(127);
    
    self->inMask = [[CVImage alloc] initWithMat: markers];
    self->gcMask = [[CVImage alloc] initWithColorMode: CVColorModeGrayScale
                                                width: w
                                               height: h];
    
    self->displayImage = [[CVImage alloc] initWithColorMode: CVColorModeBGRA
                                                      width: w
                                                     height: h];
    return self;
}

- (CGFloat) width {
    return self->image.width;
}

- (CGFloat) height {
    return self->image.height;
}

@synthesize image, inMask, gcMask, outMask, originalMask, displayImage, initialScale, initialOrientation;

- (void) convertInToGCMask {
    UInt8 *inBytes = self->inMask.mat.data;
    UInt8 *gcBytes = self->gcMask.mat.data;
    
    int width = self->inMask.width;
    int height = self->inMask.height;
    int inStride = self->inMask.stride;
    int gcStride = self->gcMask.stride;
    
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int i = inStride * y + x;
            int j = gcStride * y + x;
            
            gcBytes[j] = _displayToGrabCut(inBytes[i]);
        }
    }
}

//- (void) drawMask: (CVImageDrawingBlock) block {
//    [self->inMask draw: block];
//    [self convertInToGCMask];
//}

- (void) iterate {
    [self convertInToGCMask];
    //    if (self->outMask == nil) {
    //        self->outMask = [
    //    }
    
    cv::Mat img = image.mat;
    cv::Mat mask = gcMask.mat;
    
    cv::Mat bgd, fgd;
    int iterations = 1;
    cv::grabCut(img, mask, cv::Rect(), bgd, fgd, iterations, cv::GC_INIT_WITH_MASK);
    
    // let's get all foreground and possible foreground pixels
    cv::Mat1b mask_fgpf = ( mask == cv::GC_FGD) | ( mask == cv::GC_PR_FGD);
    // and copy all the foreground-pixels to a temporary image
    cv::Mat3b tmp = cv::Mat3b::zeros(img.rows, img.cols);
    img.copyTo(tmp, mask_fgpf);
    
    outMask = [[CVImage alloc] initWithMat: mask_fgpf];
}

- (void) saveImage: (UIImage *) image toFile: (NSString *) filename {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = paths.firstObject;
    NSString *path = [basePath stringByAppendingFormat: @"/%@", filename];
    NSLog(@"saving to %@", path);
    
    NSData *data = UIImagePNGRepresentation(image);
    [data writeToFile: path atomically: YES];
}

#pragma mark -

NS_INLINE UInt8 _clamp(SInt32 x) {
    if (x < 0)
        return 0;
    
    if (x > 255)
        return 255;
    
    return x;
}

- (void) updateDisplayImage {
    BOOL hasR = self->outMask != nil;
    
    UInt8 *iBytes = self->image.mat.data;
    UInt8 *mBytes = self->inMask.mat.data;
    UInt8 *aBytes = self->originalMask.mat.data;
    UInt8 *rBytes = (hasR) ? self->outMask.mat.data : nil;
    UInt8 *dBytes = self->displayImage.mat.data;
    
    int width = self->image.width;
    int height = self->image.height;
    
    int iStride = self->image.stride;
    int mStride = self->inMask.stride;
    int aStride = self->originalMask.stride;
    int rStride = (hasR) ? self->outMask.stride : 0;
    int dStride = self->displayImage.stride;
    
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int i = iStride * y + x * 3;
            int m = mStride * y + x * 1;
            int a = aStride * y + x * 1;
            int r = rStride * y + x * 1;
            int d = dStride * y + x * 4;
            
            SInt32 result = (hasR) ? rBytes[r] : 255;
            SInt32 alpha = result * 3 / 4 + 64;
            alpha *= aBytes[a];
            alpha /= 255;
            
            SInt32 mask = mBytes[m];
            mask -= 127;
            mask /= 2;
            
            SInt32 blue = iBytes[i+0];
            blue += mask;
            blue *= alpha;
            blue /= 255;
            
            SInt32 green = iBytes[i+1];
            green += mask;
            green *= alpha;
            green /= 255;
            
            SInt32 red = iBytes[i+2];
            red += mask;
            red *= alpha;
            red /= 255;
            
            dBytes[d+0] = _clamp(blue);
            dBytes[d+1] = _clamp(green);
            dBytes[d+2] = _clamp(red);
            dBytes[d+3] = alpha;
        }
    }
}

@end
