//
//  CVImageInternal.hpp
//  GrabCutTest
//
//  Created by Greg on 10/18/18.
//

#ifndef CVImageInternal_h
#define CVImageInternal_h

#import <opencv2/core.hpp>
#import <opencv2/imgproc.hpp>
#import <opencv2/imgproc/types_c.h>

@interface CVImage (CPP)

- (id) initWithMat: (cv::Mat) m;
@property(readonly) cv::Mat mat;

@end

#endif /* CVImageInternal_h */
